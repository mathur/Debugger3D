﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

public class CallMSCgen : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Trace.OnVariableChange += ImportImages;
    }

    // Update is called once per frame
    void ImportImages(int newVal)
    {     
        if (Trace.pointerToCurrAtomicStep > 1)
        {
            int step_num = newVal - 2;
            Debug.Log("Calling MSCgen at Step: " + (step_num));
            // create the input file with log of events to run the python script
            StreamWriter writer = new StreamWriter("C:\\Users\\redneoslocal\\Documents\\Debugger3D\\Debugger3D\\Assets\\Resources\\Step_Events.txt", false);
            foreach (string actors in Actors.allActors.Keys)
                writer.Write(actors + ",");
            writer.Write((step_num) + "\n");
            foreach (string events in NetworkInterface.json_line)
                writer.WriteLine(events);
            writer.Close();

            // execute MSCgen.py script to import step images
            string current = Directory.GetCurrentDirectory();
            Directory.SetCurrentDirectory("Assets/Resources/Models");
            System.Diagnostics.Process process = new System.Diagnostics.Process();
            process.StartInfo.FileName = "C:\\Users\\redneoslocal\\AppData\\Local\\Programs\\Python\\Python36-32\\python.exe";
            string program_name = "C:\\Users\\redneoslocal\\Documents\\Debugger3D\\Debugger3D\\Utils\\MSCgen.py";
            string file = "C:\\Users\\redneoslocal\\Documents\\Debugger3D\\Debugger3D\\Assets\\Resources\\Step_Events.txt";
            process.StartInfo.Arguments = string.Format("{0} {1}", program_name, file);
            process.StartInfo.UseShellExecute = false;
            //process.StartInfo.CreateNoWindow = true;
            //process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            process.Start();
            
            process.WaitForExit();
            Directory.SetCurrentDirectory(current);            

            // if chart is created before image is loaded then add image to chart
            if (GameObject.Find("Step " + (step_num)))
            {
                Debug.Log("Adding image to chart: " + step_num);
                AssetDatabase.Refresh();
                GameObject.Find("Step " + (step_num)).GetComponent<Image>().sprite = Resources.Load<Sprite>("Models/Outputs/Step" + (step_num));
            }
        }
    }
}
