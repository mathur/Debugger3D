﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using TMPro;
using UnityEditor;
using UnityEngine;

public class ParseCassandra : MonoBehaviour {

    List<List<string>> events = new List<List<string>>();
    public GameObject arrow_prefab, state_prefab, scaling_object, messageHandler, slider;
    public GameObject N0, N1, N2; // node prefabs

    /// <summary>
    /// Parse Input File and Store Message Data 
    /// </summary>
    void Start()
    {
        string path = EditorUtility.OpenFilePanel("Select Cassandra trace file", "", "txt");
        StreamReader reader = new StreamReader(path);
        string line;
        Regex regex = new Regex(@"^(.*)recvNode=(\d).*verb=(\w+).*sendNode=(\d).*clientRequest=(\d).*$");
        while ((line = reader.ReadLine()) != null)
        {
            Match match = regex.Match(line);
            if (match.Success)
            {   
                Regex regex_response = new Regex(@"^.*response=(\w+).*$"); // capture the response, if any
                Match match_response = regex_response.Match(match.Groups[1].Value);
                string response = "None";
                if (match_response.Success)
                {
                    response = match_response.Groups[1].Value;
                }

                List<string> event_info = new List<string>(); // store all the captured fields
                event_info.Add(response); // response text = event_info[0]
                event_info.Add(match.Groups[2].Value); // recvNode = event_info[1]
                event_info.Add(match.Groups[3].Value); // verb = event_info[2]
                event_info.Add(match.Groups[4].Value); // sendNode = event_info[3]
                event_info.Add(match.Groups[5].Value); // clientRequest = event_info[4]
                events.Add(event_info);
            }
        }

        // instantiate the nodes
        CreateStates();
    }

    /// <summary>
    /// Generate Message Sequence Chart From Parsed Data
    /// </summary>
    public void CreateStates()
    {
        int step_num = 0;
        float total_offset = 0.48f;

        // create each atomic step
        foreach (List<string> ev in events)
        {
            step_num++;
      
            // determine which nodes are the sender and receiver

            GameObject sender;
            GameObject receiver;
            switch (ev[1])
            {
                case "0":
                    N0.GetComponent<Node>().numOfStates++;
                    receiver = N0;
                    break;
                case "1":
                    N1.GetComponent<Node>().numOfStates++;
                    receiver = N1;
                    break;
                case "2":
                    N2.GetComponent<Node>().numOfStates++;
                    receiver = N2;
                    break;
                default:
                    receiver = null;
                    break;
            }
            switch (ev[3])
            {
                case "0":
                    N0.GetComponent<Node>().numOfStates++;
                    sender = N0;
                    break;
                case "1":
                    N1.GetComponent<Node>().numOfStates++;
                    sender = N1;
                    break;
                case "2":
                    N2.GetComponent<Node>().numOfStates++;
                    sender = N2;
                    break;
                default:
                    sender = null;
                    break;
            }

            // add event to receiving node's message queue 

            if (receiver.GetComponent<Node>().messageQueue == null)
            {
                receiver.GetComponent<Node>().messageQueue = new List<string>();
            }
            receiver.GetComponent<Node>().messageQueue.Add(ToString(ev[2], ev[3], ev[0], ev[4]));

            // set up sender and receiver states to denote arrow start and end points

            GameObject sender_state = new GameObject("Arrow Start Point for Step " + step_num);
            sender_state.GetComponent<Transform>().SetParent(sender.transform);
            sender_state.transform.localPosition = new Vector3(0f, total_offset - 0.15f, 0f);
            sender_state.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
            sender_state.transform.localScale = new Vector3(1f, 1f, 1f);

            GameObject receiver_state = new GameObject("Arrow End Point for Step " + step_num);
            receiver_state.GetComponent<Transform>().SetParent(receiver.transform);
            receiver_state.transform.localPosition = new Vector3(0f, total_offset - 0.2f, 0f);
            receiver_state.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
            receiver_state.transform.localScale = new Vector3(1f, 1f, 1f);

            total_offset = receiver_state.transform.localPosition.y;

            // add arrow body between sender and receiver states

            GameObject arrow;
            // create self-directed arrow when node sends message to itself
            if (sender == receiver)
            {
                // set up bezier points to draw curve
                BezierCurve bezier_curve = sender.AddComponent<BezierCurve>();

                BezierPoint p1 = bezier_curve.AddPointAt(new Vector3(0f, 0f, 0f));
                p1.transform.localPosition = sender_state.transform.localPosition;
                p1.handle1 = new Vector3(-20f, 0f, 0f);

                BezierPoint p2 = bezier_curve.AddPointAt(new Vector3(0f, 0f, 0f));
                p2.transform.localPosition = receiver_state.transform.localPosition;
                p2.handle1 = new Vector3(20f, 0f, 0f);

                // map bezier points and draw curve using line renderer
                arrow = Instantiate(arrow_prefab);
                arrow.GetComponent<Transform>().SetParent(sender_state.transform);
                arrow.transform.localPosition = new Vector3(0f, 0f, 0f);
                arrow.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
                arrow.transform.localScale = new Vector3(1f, 1f, 1f);
                int lengthOfLineRenderer = 30;
                LineRenderer lineRenderer = arrow.GetComponent<LineRenderer>();
                lineRenderer.positionCount = lengthOfLineRenderer;
                lineRenderer.widthMultiplier = 2f;
                var points = new Vector3[lengthOfLineRenderer];               
                for (int i = 0; i < lengthOfLineRenderer; i++)
                {
                    points[i] = bezier_curve.GetPointAt(i / (float)(lengthOfLineRenderer - 1));
                }
                lineRenderer.SetPositions(points);
            }
            // create normal arrow body
            else
            {
                arrow = Instantiate(arrow_prefab);
                arrow.GetComponent<Transform>().SetParent(sender_state.transform);
                arrow.transform.localPosition = new Vector3(0f, 0f, 0f);
                arrow.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
                arrow.transform.localScale = new Vector3(1f, 1f, 1f);
                arrow.GetComponent<LineRenderer>().SetPosition(0, sender_state.transform.position);
                arrow.GetComponent<LineRenderer>().SetPosition(1, receiver_state.transform.position);
                // make the arrow thinner towards the receiver node
                AnimationCurve curve = new AnimationCurve();
                curve.AddKey(0f, 0.4f);
                curve.AddKey(0.8f, 0.4f);
                curve.AddKey(0.9f, 0.2f);
                curve.AddKey(1f, 0.2f);
                arrow.GetComponent<LineRenderer>().widthCurve = curve;
                arrow.GetComponent<LineRenderer>().widthMultiplier = 4f;
            }

            // create slanted arrow heads

            if (String.Compare(sender.name, receiver.name) < 0) // arrow head for downward-pointing arrow
            {
                // create left arrow mark
                GameObject left_arrow = Instantiate(arrow_prefab);
                left_arrow.GetComponent<Transform>().SetParent(receiver_state.transform);
                left_arrow.transform.localPosition = new Vector3(0f, 0f, 0f);
                left_arrow.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
                left_arrow.transform.localScale = new Vector3(1f, 1f, 1f);
                left_arrow.GetComponent<LineRenderer>().SetPosition(0, receiver_state.transform.position);
                Vector3 left_pos = new Vector3(receiver_state.transform.position.x - 8f, receiver_state.transform.position.y + 0.2f, receiver_state.transform.position.z + 1f);
                left_arrow.GetComponent<LineRenderer>().SetPosition(1, left_pos);
                left_arrow.GetComponent<LineRenderer>().startWidth = 0.6f;
                left_arrow.GetComponent<LineRenderer>().endWidth = 0.6f;
                
                // create right arrow mark
                GameObject right_arrow = Instantiate(arrow_prefab);
                right_arrow.GetComponent<Transform>().SetParent(receiver_state.transform);
                right_arrow.transform.localPosition = new Vector3(0f, 0f, 0f);
                right_arrow.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
                right_arrow.transform.localScale = new Vector3(1f, 1f, 1f);
                right_arrow.GetComponent<LineRenderer>().SetPosition(0, receiver_state.transform.position);
                Vector3 right_pos = new Vector3(receiver_state.transform.position.x - 8f, receiver_state.transform.position.y + 0.2f, receiver_state.transform.position.z - 4f);
                right_arrow.GetComponent<LineRenderer>().SetPosition(1, right_pos);
                right_arrow.GetComponent<LineRenderer>().startWidth = 0.6f;
                right_arrow.GetComponent<LineRenderer>().endWidth = 0.6f;
            }
            else if (String.Compare(sender.name, receiver.name) > 0) // arrow head for upward-pointing arrow
            {
                // create left arrow mark
                GameObject left_arrow = Instantiate(arrow_prefab);
                left_arrow.GetComponent<Transform>().SetParent(receiver_state.transform);
                left_arrow.transform.localPosition = new Vector3(0f, 0f, 0f);
                left_arrow.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
                left_arrow.transform.localScale = new Vector3(1f, 1f, 1f);
                left_arrow.GetComponent<LineRenderer>().SetPosition(0, receiver_state.transform.position);
                Vector3 left_pos = new Vector3(receiver_state.transform.position.x + 8f, receiver_state.transform.position.y + 1f, receiver_state.transform.position.z - 5f);
                left_arrow.GetComponent<LineRenderer>().SetPosition(1, left_pos);
                left_arrow.GetComponent<LineRenderer>().startWidth = 0.6f;
                left_arrow.GetComponent<LineRenderer>().endWidth = 0.6f;

                // create right arrow mark
                GameObject right_arrow = Instantiate(arrow_prefab);
                right_arrow.GetComponent<Transform>().SetParent(receiver_state.transform);
                right_arrow.transform.localPosition = new Vector3(0f, 0f, 0f);
                right_arrow.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
                right_arrow.transform.localScale = new Vector3(1f, 1f, 1f);
                right_arrow.GetComponent<LineRenderer>().SetPosition(0, receiver_state.transform.position);
                Vector3 right_pos = new Vector3(receiver_state.transform.position.x + 8f, receiver_state.transform.position.y + 0.2f, receiver_state.transform.position.z + 2f);
                right_arrow.GetComponent<LineRenderer>().SetPosition(1, right_pos);
                right_arrow.GetComponent<LineRenderer>().startWidth = 0.6f;
                right_arrow.GetComponent<LineRenderer>().endWidth = 0.6f;
            }
            else // arrow head for self-directed arrow
            {
                // create left arrow mark
                GameObject left_arrow = Instantiate(arrow_prefab);
                left_arrow.GetComponent<Transform>().SetParent(receiver_state.transform);
                left_arrow.transform.localPosition = new Vector3(0f, 0f, 0f);
                left_arrow.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
                left_arrow.transform.localScale = new Vector3(1f, 1f, 1f);
                left_arrow.GetComponent<LineRenderer>().SetPosition(0, receiver_state.transform.position);
                Vector3 left_pos = new Vector3(receiver_state.transform.position.x + 8f, receiver_state.transform.position.y + 1f, receiver_state.transform.position.z - 3f);
                left_arrow.GetComponent<LineRenderer>().SetPosition(1, left_pos);
                left_arrow.GetComponent<LineRenderer>().startWidth = 0.6f;
                left_arrow.GetComponent<LineRenderer>().endWidth = 0.6f;

                // create right arrow mark
                GameObject right_arrow = Instantiate(arrow_prefab);
                right_arrow.GetComponent<Transform>().SetParent(receiver_state.transform);
                right_arrow.transform.localPosition = new Vector3(0f, 0f, 0f);
                right_arrow.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
                right_arrow.transform.localScale = new Vector3(1f, 1f, 1f);
                right_arrow.GetComponent<LineRenderer>().SetPosition(0, receiver_state.transform.position);
                Vector3 right_pos = new Vector3(receiver_state.transform.position.x + 8f, receiver_state.transform.position.y + 0.2f, receiver_state.transform.position.z + 2f);
                right_arrow.GetComponent<LineRenderer>().SetPosition(1, right_pos);
                right_arrow.GetComponent<LineRenderer>().startWidth = 0.6f;
                right_arrow.GetComponent<LineRenderer>().endWidth = 0.6f;
            }

            // create message handlers to hold messages in queue
            GameObject messageQueue = Instantiate(messageHandler);
            arrow.AddComponent<BoxCollider>();
            arrow.GetComponent<ShowQueue>().messageHandler = messageQueue;
            arrow.GetComponent<Transform>().SetParent(null, true);
            messageQueue.GetComponent<Transform>().SetParent(receiver_state.transform);
            messageQueue.transform.localPosition = new Vector3(0f, 0f, -41f);
            messageQueue.transform.localRotation = Quaternion.Euler(0f, 0f, 0f);
            messageQueue.transform.localScale = new Vector3(3.84f, 0.05f, 22.28f);
            messageQueue.GetComponent<Transform>().SetParent(null, true);
            messageQueue.GetComponent<Renderer>().enabled = false;
            int msgNum = 0;
            foreach (String message in receiver.GetComponent<Node>().messageQueue)
            {
                msgNum++;
                messageQueue.GetComponent<Transform>().GetChild(0).GetComponent<TextMeshPro>().text += msgNum + ": " + message + "\n";
                messageQueue.GetComponent<Transform>().GetChild(0).GetComponent<TextMeshPro>().enabled = false;
            }

        }
        
       // adjust the scale of the chart and slider accordingly based on the number of states/arrows
       scaling_object.transform.localScale = new Vector3(1f, 1f, scaling_object.transform.localScale.z + (step_num * 0.2f));
       slider.transform.localScale = new Vector3(1f, scaling_object.transform.localScale.y + (step_num * 0.14f), 1f);
    }

    // generate string representation of messages to display in queue
    public string ToString(string verb, string sendNode, string response, string clientRequest) 
    {
        return "Message: " + verb + ", Sender: " + sendNode + ",  Response: " + response + ", Client Request: " + clientRequest;
    }
}

