﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChooseVersion : MonoBehaviour {

    public GameObject Chart_images;
    private string input = "Offline";

	// Use this for initialization
	void Start () {
		if (input == "Online")
        {
            Chart_images.GetComponent<CallMSCgen>().enabled = true;
            Chart_images.GetComponent<ChartMaker>().version = "Outputs";
        }
        else if (input == "Offline")
        {
            Chart_images.GetComponent<CallMSCgen>().enabled = false;
            Chart_images.GetComponent<ChartMaker>().version = "Charts";
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
