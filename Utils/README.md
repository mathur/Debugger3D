Message Sequence Chart Generator Tool for Dinning-Hakkers
=========================

A Python script that uses MSCgen to generate:

- PNG images of message sequence charts depicting each atomic step during the dining-hakkers execution (e.g. Step#.png)
- PNG image of message sequence chart depicting entire dining-hakkers program execution (e.g. All Steps.png)
- Equivalent MSCgen input file for both individual steps and entire program execution (e.g. Step#.txt and Output.txt respectively)

The program takes as input a log file of all events taking place during the dining-hakkers app execution, obtained in the "out" folder after
termination of the dining-hakkers app. The program takes the log file as a single parameter input from the command line, e.g. "MSCgen.py allEvents.txt"    

### Requirements:

- Python 3.6
- MSC-generator 6.3.4
- Log text file 