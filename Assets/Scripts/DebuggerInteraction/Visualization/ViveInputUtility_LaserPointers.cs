﻿using HTC.UnityPlugin.Pointer3D;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ViveInputUtility_LaserPointers : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Physics2DRaycastMethod.OnVariableChange += TransformPointers;
    }

    // Update is called once per frame
    void TransformPointers (Transform transform) {
        UserInputHandler.laserPointedActor = transform;
        UserInputHandler.laserPointedCollider = transform;
    }
}
