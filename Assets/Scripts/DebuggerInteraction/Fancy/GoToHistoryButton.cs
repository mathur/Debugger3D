﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoToHistoryButton : MonoBehaviour {

    public int chart_num; //index of the dispatcher


    public void HandleClick(int indexOfDispatcher)
    {
        Debug.Log("HandleClick was called");
        StepRequest sr = new StepRequest(indexOfDispatcher);
        NetworkInterface.HandleRequest(sr);
    }
}
