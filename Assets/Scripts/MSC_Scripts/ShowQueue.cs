﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ShowQueue : MonoBehaviour {

    public GameObject messageHandler = null;

    // Use this for initialization
    void Start () {
        
    }

    // make the state appear/disappear when the corresponding arrow is clicked
    public void OnMouseDown()
    {
        if (messageHandler.GetComponent<Renderer>().enabled == true)
        {
            messageHandler.GetComponent<Renderer>().enabled = false;
            messageHandler.GetComponent<Transform>().GetChild(0).GetComponent<TextMeshPro>().enabled = false;
        }
        else if (messageHandler.GetComponent<Renderer>().enabled == false)
        {
            messageHandler.GetComponent<Renderer>().enabled = true;
            messageHandler.GetComponent<Transform>().GetChild(0).GetComponent<TextMeshPro>().enabled = true;
        }
    }
}
