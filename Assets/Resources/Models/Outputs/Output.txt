msc {
hscale = "1";
a [label = ""],
b [label = "philosopher-A"],
c [label = "philosopher-B"],
d [label = "philosopher-C"],
e [label = "philosopher-D"],
f [label = "philosopher-E"],
g [label = "chopstick-1"],
h [label = "chopstick-2"],
i [label = "chopstick-3"],
j [label = "chopstick-4"],
k [label = "chopstick-5"];
--- [label = "Step 15"];
d box d [label = "Message: Taken(chopstick-3)\nSender: chopstick-3"];
d -> d [label = "Think"];
}