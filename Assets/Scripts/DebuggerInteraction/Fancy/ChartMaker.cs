﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine.UI;
using UnityEditor;
using System.IO;

public class ChartMaker : MonoBehaviour
{
    private int num_to_get_chart = 0; // keeps track of which chart image to load from sprites
    private int chart_count = 0; // keeps track of how many charts currently exist
    public GameObject chart_prefab; //set in inspector
    public GameObject panel; // panel where list of charts are placed
    public GameObject chart_image; // where each new image is added
    public string version = "Outputs";

    void Start()
    {
        Trace.OnVariableChange += CreateNewStep;
    }

    void CreateNewStep(int newVal)
    {
        if (Trace.pointerToCurrAtomicStep > 1)
        {
            UnityEngine.Debug.Log("Chart Num: " + num_to_get_chart + ", Atomic Step Pointer: " + Trace.pointerToCurrAtomicStep + ", newVal: " + newVal);
            //// create the input file with log of events to run the python script
            //StreamWriter writer = new StreamWriter("C:\\Users\\redneoslocal\\Documents\\Debugger3D\\Debugger3D\\Assets\\Resources\\Step_Events.txt", false);
            //foreach (string actors in Actors.allActors.Keys)
            //    writer.Write(actors + ",");
            //writer.Write(num_to_get_chart + "\n");
            //foreach (string events in NetworkInterface.json_line)
            //    writer.WriteLine(events);
            //writer.Close();
            ////AssetDatabase.Refresh();

            //// execute MSCgen.py script to import step images
            //string current = Directory.GetCurrentDirectory();
            //Directory.SetCurrentDirectory("Assets/Resources/Models");
            //System.Diagnostics.Process process = new System.Diagnostics.Process();
            //process.StartInfo.FileName = "C:\\Users\\redneoslocal\\AppData\\Local\\Programs\\Python\\Python36-32\\python.exe";
            //string program_name = "C:\\Users\\redneoslocal\\Documents\\Debugger3D\\Debugger3D\\Utils\\Test.py";
            //string file = "C:\\Users\\redneoslocal\\Documents\\Debugger3D\\Debugger3D\\Assets\\Resources\\Step_Events.txt";
            //process.StartInfo.Arguments = string.Format("{0}", program_name);
            //process.StartInfo.UseShellExecute = false;
            //process.StartInfo.CreateNoWindow = true;
            //process.StartInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Hidden;
            //process.Start();
            //process.WaitForExit();
            //Directory.SetCurrentDirectory(current);
            //AssetDatabase.Refresh();



            // instantiate and set up the imported images
            GameObject chart = Instantiate(chart_prefab);
            chart.GetComponent<RectTransform>().SetParent(chart_image.transform); // set it as a child of the scroll area object
            Vector3 newPosition = new Vector3(chart.transform.position.x, chart.transform.position.y, 0f); // offset the z position
            chart.transform.localPosition = newPosition;
            chart.transform.localScale -= new Vector3(0.25F, 0.25f, 0); // offset the x and y scales
            AssetDatabase.Refresh();
            chart.GetComponent<Image>().sprite = Resources.Load<Sprite>("Models/" + version + "/Step" + num_to_get_chart); // add image to panel
            chart.name = "Step " + num_to_get_chart; // name the chart based on corresponding step number

            // make the scrollbar jump to the latest image
            Canvas.ForceUpdateCanvases();
            chart_image.GetComponent<VerticalLayoutGroup>().CalculateLayoutInputVertical();
            chart_image.GetComponent<ContentSizeFitter>().SetLayoutVertical();
            panel.GetComponent<ScrollRect>().content.GetComponent<VerticalLayoutGroup>().CalculateLayoutInputVertical();
            panel.GetComponent<ScrollRect>().content.GetComponent<ContentSizeFitter>().SetLayoutVertical();
            panel.GetComponent<ScrollRect>().verticalNormalizedPosition = 0f;
            chart_count++; // increment chart counter


            // create history button over top of image
            Button button = chart.GetComponent<Button>();
            chart.GetComponent<GoToHistoryButton>().chart_num = num_to_get_chart;
            button.onClick.AddListener(() =>
            {
                //destroy charts up until chart.GetComponent<GoToHistoryButton>().chart_num and create them again starting from here
                int temp = chart_count;
                for (int i = chart.GetComponent<GoToHistoryButton>().chart_num + 1; i <= temp; i++)
                {
                    Destroy(GameObject.Find("Step " + i));
                    chart_count--;
                }
                chart.GetComponent<GoToHistoryButton>().HandleClick(chart.GetComponent<GoToHistoryButton>().chart_num + 2);
                num_to_get_chart = chart.GetComponent<GoToHistoryButton>().chart_num + 1;
            });

            num_to_get_chart++;
        }
    }
}
