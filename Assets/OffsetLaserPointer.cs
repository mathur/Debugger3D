﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffsetLaserPointer : MonoBehaviour {

    public GameObject RightLaserPointer;
    public GameObject CameraRig;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        RightLaserPointer.transform.position = CameraRig.transform.position;
	}
}
