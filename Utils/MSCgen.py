import re
import json
import subprocess
import os
import sys

data = []
actors = []
philosophers = []
chopsticks = []
step_num = -1

# Load each JSON string
with open(sys.argv[1]) as f:
    actor_list = f.readline()
    for line in f:
        data.append(json.loads(line))
    actor_list = actor_list.split(",")
    for actor in actor_list[:-1]:
        if actor.split("-")[0] == "philosopher":
            philosophers.append(actor)
        elif actor.split("-")[0] == "chopstick":
            chopsticks.append(actor)
    step_num = actor_list[-1].rstrip()
    actors = sorted(philosophers) + sorted(chopsticks)
    actors.insert(0, "deadLetters")


# Assign each actor its own MSCgen ascii character variable, starting from lower case letter 'a'
var = {}
alphabet = 97
for actor in actors:
    var[actor] = chr(alphabet)
    alphabet = alphabet + 1

# Create a new folder called "Outputs" to save all the output files
path = os.getcwd() + "/Outputs"
if not os.path.exists(path):
    os.makedirs(path)
os.chdir(path)

# Write the output to the MSCgen input file
output_file = open("Output.txt", "w")  # create output file with all MSCgen commands
declaration = "msc {\nhscale = \"1\";\n"  # header statements to set up a new MSCgen file
# Declare list of actors based on assigned variable names
for actor in actors[:-1]:
    if actor == "deadLetters":
        declaration = declaration + var[actor] + " [label = \"\"],\n"  # make the deadLetters box nameless
    else:
        declaration = declaration + var[actor] + " [label = \"" + actor + "\"],\n"
else:
    last_actor = actors.pop()
    declaration = declaration + var[last_actor] + " [label = \"" + last_actor + "\"];\n"
output_file.write(declaration)
if int(step_num) == 0:
    output_file.write("--- [label = \"Step0\"];\n")
# Write each JSON step (after step 0) to output file as a MSCgen connection between actors
for x in range(0, len(data)):
    # write message receives to output file
    if data[x]['eventType'] == "MESSAGE_RECEIVED":
        output_file.write("--- [label = \"Step " + step_num + "\"];\n")
        output_file.write(var[data[x]['receiverId']] + " box " + var[data[x]['receiverId']] +
                          " [label = \"Message: " + data[x]['msg'] + "\\nSender: " + data[x]['senderId'] + "\"];\n")
    # write message sends to output file
    if data[x]['eventType'] == "MESSAGE_SENT":
        output_file.write(var[data[x]['senderId']] + " -> " + var[data[x]['receiverId']] +
                          " [label = \"" + data[x]['msg'] + "\"];\n")
# call MSCgen on the final step file
output_file.write("}")
output_file.close()
subprocess.call(["mscgen", "-o", "Step" + step_num + ".png", "-Ssignalling", "Output.txt"])





