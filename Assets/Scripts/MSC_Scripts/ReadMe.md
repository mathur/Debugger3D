Unity MSC
=========================

This message sequence chart (MSC) generator creates a 2D MSC in Unity based on traces of the Cassandra program. 

### Requirements:

- Unity 2018.2.1f1
- Cassandra trace text files

User Guide:

Upon execution in Unity, the user will be prompted to select the trace file (*.txt) that they wish to analyze.
The program will then generate arrows corresponding to the atomic steps parsed from the file. The arrows are directed
from the sending node to receiving node. Each of the three nodes are labelled, starting from Node 0 at the top, with the red state 
representing the default start state. Once the chart is generated, the user can scroll through the atomic steps and query the 
state of the node at each atomic step. This is done by clicking on the body of the arrow corresponding to that atomic step.
Subsequently, the message queue for that state will appear with the messages listed in order of arrival/processing and display
the relevant message information (Verb/message sent, the node that sent the message, the response if any, and the client request). 
To hide the message queue simply click on the arrow body again. Note that the messages may not appear directly over top of the
message handler/box unless scrolled over to that corresponding atomic step. 