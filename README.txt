Once you have the back-end set up, start the server, followed by running the executable Build/StableBuild.exe
This shall start a debug session in 3D Virtual Reality (currently supported on HTC Vive).
The project can also be opened inside Unity--- use appropriate scene from Assets\Scenes\. 
The project has been tested on Unity 2017. 
All Scripts in Assets\Scripts\