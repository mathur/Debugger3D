﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Slide_Actors : MonoBehaviour {

    public GameObject chart;

    // move chart as slider is dragged
    public void Change_Slider(float newVal)
    {
        Vector3 pos = chart.transform.position;
        pos.z = newVal * 1200;
        chart.transform.position = pos;

    }
}
